Drohnen in der Forstwirtschaft, Kurs-Nr. 1000175, Abt. Waldinventur und Fernerkundung, Universität Göttingen, WiSe 2023/2024

Dr. Hans Fuchs, hfuchs@gwdg.de

# Erkennung von Borkenkäferbefall in Fichtenbeständen mithilfe multispektraler Drohnenbilder


## Hintergrund
Seit 2019 verursacht eine Borkenkäferkalimität (*Ips typographus*) großflächige Veränderungen in Fichtenwäldern des Harzes. In einem Pilotprojekt der Abteilung Waldinventur und Fernerkundung, Universität Göttingen, wurden Drohnenflüge im niedersächsischen Landesforstamt Clausthal durchgeführt. Optische Bilddaten wurden während einer Befliegung am 10.05.2022 mit RGB and multispektrale Kameras (s.Tab. 1) aufgenommen. Digitale Orthomosaike wurden erstellt und in einem WebGIS bereitgestellt (Fuchs, Nölke und Magdon (2022), http://wwwuser.gwdg.de/~hfuchs/altenau/). 

Tabelle 1. Spektrale Auflösung der multispektralen RedEdge-MX Kamera (Micasense).

| Bandno.   | Spektral range  | Wavelenght [nm]|
| -------- | --------    | -------- |
|1         | Blue        | 475      |
|2         | Green       | 566      |
|3         | Red         | 668      |
|4         | RedEdge     | 717      |
|5         | Near IR     | 842      |



## Voraussetzung
Ein Laptop oder PC mit mindestens 8GB RAM, ca. 4GB freien Platz auf der Festplatte, eine Maus und ein 64bit Betriebssystem.

## Installation und Konfiguration

### Installation von Git for Windows
- Gehe auf die "Git for Windows" Homepage https://gitforwindows.org und lade die aktuelle Version für dein Betriebssystem herunter. 
- Akzeptiere die Standardeinstellungen während der Installation.

### Installation von Git für macOSX
- Lade die neue Version von Git herunter. https://sourceforge.net/projects/git-osx-installer/files
- Finde die heruntergeladene Datei und Doppelklick um den Git Installer zu öffnen.
- Folge den Anweisungen bis die Installation beendet ist.
- Öffne die Terminal app and gebe ein:

```git --version```

### Installation von Miniforge für Windows
Verwende die freie und quelloffenene Distribution "Miniforge" für die Installation von Python. Sie enthält die Python Paketmanagement-Systeme "conda" und "mamba", die mit dem nicht-kommerziellen Kanal "conda-forge" verbunden sind.

- Öffne die Miniforge GitHub Seite https://github.com/conda-forge/miniforge

- Lade den Windows x86_64 Installer für Miniforge3 herunter: Windows x86_64 Installer

- Führe den Windows installer aus. Akzeptiere die Standardeinstellungen.

Nachdem die Miniforge Installation beendet ist, öffne einen Terminal:
- Start > Miniforge > Miniforge Prompt. "(base)" erscheint vor dem Shell prompt. Wechsele in das Verzeichnis in dem die Dateien gespeichert werden sollen. Gebe in das Miniforge Prompt Terminal ein:

```cd C:\Users\<yourusername>\Documents```


### Installation von Miniforge für macOSX

- Öffne das Apple Menü und wähle "About This Mac". Klicke auf den "System Report" Knopf. Checke auf welcher Architektur dein macOSX läuft: CPU Type: Intel-Core = x86_64 oder CPU Type: M1 = arm64 (Apple Silicon)?

- Öffne die Miniforge GitHub Seite https://github.com/conda-forge/miniforge

- Lade den passenden Miniforge3 Installer für deine OSX Architektur herunter oder öffne einen Terminal, copy und paste die Kommandozeile (ohne Zeilenumbruch!) passend für deinen CPU-Typ:
    - für Intel-Core CPU:
```curl -L -O "https://github.com/conda-forge/miniforge/releases/latest/download/Miniforge3-MacOSX-x86_64.sh"```

    - für M1 CPU:

```curl -L -O https://github.com/conda-forge/miniforge/releases/latest/download/Miniforge3-MacOSX-arm64.sh```

- Wechsle in den Downloads Ordner:
```cd Downloads```
- Gebe in das Terminal ein:
```bash Miniforge3-MacOSX-$(uname -m).sh```

- Folge den Anweisungen am Bildschirm.

    - Tippe "yes" um die Lizenzbedingungen zu bestätigen.
    - Drücke Enter um den Installationsort zu bestätigen.
    - Tippe "yes" um den Miniforge Installer zu initialiseren.

- Wenn conda init fertig ist, Schliessen und Neustart des Terminals.
- Wenn die Installation erfolgreich war starte einen neuen Terminal. Es erscheint "(base)" vor dem Befehlsprompt im Terminal,
-  Tippe in das Terminal (Ersetze yourusername mit deinem Benutzernamen):
```cd /Users/<yourusername>/Documents```


### Klonen dieses Repository: 
- Gebe in das Terminal ein:
```git clone https://gitlab.gwdg.de/hfuchs/uasens.git```

- Wechsele in das Verzeichnis uasens. Gebe in das Miniforge Prompt Terminal ein:
```cd uasens```

- Erstelle eine neue virtuelle Umgebung und installiere alle notwendigen Python Pakete unter Verwendung einer Konfiguration-Datei YAML. Gebe in das Miniforge Prompt Terminal ein:
```mamba env create --file uasens.yml```

### Öffnen der Notebooks
- Nach Abschluss der Installtion aktiviere die virtuelle Umgebung "uasens"
```conda activate uasens```

- Zum Öffnen von Jupyterlab gebe ein:
```jupyter lab```

- Öffne das Notebook "02_Datenvorbereitung.ipynb" in Jupyterlab.
    
